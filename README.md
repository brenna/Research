# Research

Work group that is conducting research within the network. More edits to come for this Read Me. ;) 


## Goals 
To better understand:
– Infrared members’ individual and collective needs and wishes
– where Infrared can meet external needs elsewhere by developing a map of the ecosystem it works within, constituted by several like-minded and often intersecting communities, such as RaReNet (Rapid Response Network), CiviCERT and other networks and coalitions.

This will allow the Infrared network to most strategically develop and implement its initiatives as a key actor and partner in the independent Internet infrastructure provider sector.

!! However, participation by Infrared members in this research project is entirely voluntary!
!! And, Infrared members comprise the research team, too so we invite you to join the research team.

So to those ends, we have to goals for the research:
 1. Scope the current composition of Infrared, highlighting values alignment, needs and requirements that can enable action within the network for future shared projects.
 2. Map the landscape of digital human rights, internet freedom, and internet governance fora and coalitions where the Infrared network has external potential for collective action.

# Meetings

Aiming for every two weeks. Notes from meeting are [here.](https://0xacab.org/infrared/members/Research/-/tree/no-masters/meetings?ref_type=heads)
