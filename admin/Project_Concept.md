Concept: Scoping & Mapping Infrared Project

21 July 2023  
Beatrice Martini & Mallory Knodel

Audience: shared with funders

# OVERVIEW

Infrared is a network of Internet infrastructure providers from around the world that provide alternatives to multinational corporate platforms to underserved, underrepresented and at-risk communities of advocates, activists, human rights defenders and journalists and media workers.

The aim of the member organizations involved in Infrared is to share knowledge about how to most effectively and sustainably serve the communities and individuals they strive to support, thus strengthening their own operations as well as contributing to collective initiatives ultimately boosting each other’s capabilities and resources.

For this purpose, it is key for the Infrared network to:

- gather a clear and in-depth understanding of its members’ individual and collective needs and wishes

- clarify where Infrared can meet needs elsewhere by developing a map of the ecosystem it works within, constituted by several like-minded and often intersecting communities, such as RaReNet (Rapid Response Network), CiviCERT and other networks and coalitions.

This will allow the Infrared network to most strategically develop and implement its initiatives as a key actor and partner in the independent Internet infrastructure provider sector.

However, participation by Infrared members in this research project is entirely voluntary!

And, Infrared members comprise the research team, too– see Next Steps below.

# GOALS

In order to address the needs outlined above, the goals of this research are to:

1. Scope the current composition of Infrared, highlighting values alignment, needs and requirements that can enable action within the network for future shared projects.
    
2. Map the landscape of digital human rights, internet freedom, and internet governance fora and coalitions where the Infrared network has external potential for collective action.
    

# RESEARCH QUESTIONS

In order to scope the current composition of Infrared, as well as its capabilities, gaps and potential opportunities for further growth we will ask participants who have opted into this research project the following questions as well as conduct our own inquiries:

- What are the roles that best describe the work done by the members of your organization?
    
- Which competencies are represented in your organization?
    
- Which competencies are not represented in your organization, why this is the case, and which would you prioritize acquiring?
    
- In your opinion/ to the extent of your knowledge, which of the above are well represented in terms of capacity across the Infrared network? Which are missing and would be important to acquire?
    
- What are shared projects that your organization would find most relevant to prioritize and collaborate on, leveraging the connections built through Infrared?
    
- In your opinion, how can we ensure that the initiatives carried out across Infrared are equitable? In this regard, we welcome your input on matters of sustainability, time availability, as well as compensation and incentives models.
    

In order to map the landscape of digital human rights, internet freedom, and internet governance fora and coalitions where the Infrared network has external potential for collective action, we will ask participants who have opted into this research project the following questions as well as conduct our own inquiries:

- In your opinion, what are the key actors (e.g. individual or networks/coalitions/fora of organizations, practitioners, service providers) operating across the ecosystem Infrared is part of?
    
- Are there collaboration opportunities with the aforementioned actors that, in your opinion, would be most impactful for the services that the Infrared network strives to provide? If so, what are they, and how would you envision exploring them?
    

# METHODS

- Collect qualitative descriptive data to answer the research questions, through survey and/or interviews
    
- Conduct qualitative analysis of the collected data
    
- Collect primary and secondary sources through supportive desk research
    
- Summarize findings in a first version of the research
    
- Share and discuss the first version of the research with network members in order to develop a community-owned final version.
    

# MILESTONES

## Research proposal, August 2023

Agreement on the project. Mallory and Beatrice fundraise for the work.

20 hours

## Research methods, September-October 2023

Develop the Survey. Conduct Interviews. Scope the landscape with desk research. Analyze results. Write an initial draft report. 

200 hours

## Review, November 2023

Respondents will be asked to review for redactions. Broadly, Infrared members’ reflections will be gathered and incorporated. Launch at NPDev Summit. Publish more widely. 

80 hours

# NEXT STEPS

Now: Please send any comments or concerns about this proposal to the list or directly to Mallory and Beatrice within a week. 

Also: If you would like to join the research team, please let us know. 

Next we will fundraise for the report and the researchers’ time. 

After the report is ready we could use it to form projects or prepare an external publication.

# Funding request

_Update: In January 2024 Ford Foundation awarded Exchange Point Institute (Mallory's consulting 501c3) with $50,000 for the work as outlined in this concept note to be completed by the end of December 2024._
