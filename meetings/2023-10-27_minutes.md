# 27 October 2023
Present: 
Mallory
Gunner
Beatrice
Brenna

# Agenda
- Questions about workspace for research (I put some thoughts below (brenna)
- Updates from previous meeting (including review of action points)
- Plans for infrared meeting and Dev Summit in November
- Sending an update to the infrared list

# Notes

## Questions about workspace for research
- Agreed: We use etherpad/cryptpads for meeting notes, to be stored in 0xacab
   
#todo address where to have documents/data live later

## Tasks from prior meetings
- [x] Create a project in 0xacab – Brenna
    - [x] Move over Project proposal – Brenna
    - [x] Move over Research plan

- [ ] Pull down infrared list membership and compare to the listed members in 0xacab – TBD
(most likely there are more folks on the list than on 0xacab– TBD
https://0xacab.org/groups/infrared/members/-/group_members

- [ ] Review research plan, make comments and suggestions (see “TODO” items in yellow)  – All

- Apply to:
    - Digital Infrastructure Fund – Mallory
    - Agency Fund https://www.agency.fund/apply – Georgia
    - FIRN https://www.apc.org/en/news/feminist-internet-research-network-call-research-proposals-1 – Mallory
    - OTF IF Fund – TBD

### Update from Ford meeting

Mallory: Update from Ford meeting – Michael Brennan could support from Q1 (shorter term)
Other potential short term support: OTF, depending on open calls

Mallory: ISOC: pointed to the right direction of the right officer to tlak with in the future. Unless they have an open call soon, short term not a useful contact. But promising for more sizeable support in the long term

## Infrared meeting @ Oakland: to be discussed
What is the governance model we need and want? Affirming that what is documented in 0axacab is correct and anything else we want to develop further

Which tools/services provided by which org in networks?

Tues November 14: expected 15-20 people

From our research group: 
- Gunner, Beatrice. Brenna/Mallory can’t go, Georgia TBD
- Jaime and a colleague from mayfirst
- invited Connor Bedard EFF
- Elijah riseup

### What to discuss

If there is something we want to discuss further during the week, we can take time during Dev Summit

How to make more visible the services provided by infrared members
- Example: https://digitalfirstaid.org/en/support/ 

Process: emailed each member with a number of questions and lists of services (also languages), so that they could easily give an overview of their offer


## Workspace considerations thoughts
I held off on setting up more stuff in the 0xacab space. I have some questions about where the work will happen. I find 0xacab to be very overwhelming for non-coding collaboration work (spreadsheets, formatted docs, etc) so that's the background on my questions/ideas.
I'd like more clarity about what our needs/preferences are for the workspace, tasks and data storage.

TLDR:
1. We use NextCloud or Cryptdrive for the research work (drafting, task tracking) and use 0xacab primarily for final versions of documents, meeting notes, and things that need InfraRed member review, etc. If needed, tasks/issues could live in 0xacab.
2. If you have an existing workflow/ system for these types of research projects that you think works well, we could also use a version of that. I'm not sure how you've (work group members) done these types of project before so
3. Ideally, not using G-suite/docs (anymore than necessary) would be the broader goal.

### workspace

Using gitlab/0xacab for format rich documents and spreadsheets is not great. For example, re-creating the "timeline & activities" table from the Gdoc is laborious/impossible (at least with my markdown skills).

With either NextCloud or CryptDrive we could have similar functionality as a google workspace but wouldn't have to use google for the project.

Cryptdrive is pretty simple to set up. Here's some info about collaboration features: https://docs.cryptpad.org/en/user_guide/collaboration.html  
Problems with Cryptpad: not part of InfraRed, limited storage, etc.

I haven't used NextCloud in a while and am not up on which InfraRed project offers this service. So could be a slower route to setting up a work space but seems feasible.

**I think an InfraRed hosted NextCloud space would work best as the hosting/service would be InfraRed run.** The need would be to find the org/folks to support the set up.

### task tracking

For task tracking: this could be done in 0xacab, Nextcloud or Cryptdrive.

My preference is to have all the issue/task tracking/project management in one workspace as much as possible but I also understand the need to balance ease/usability with network transparency/involvement (0xacab as the shared work place for all InfraRed members).

Keeping tasks in 0xacab probably works better for transparency and easier access for potential newcomers from infrared who'd like to get involved in the research project. Notifications are already included with however you set up your 0xacab account

I haven't use NextCloud in a while but it does have some sort of task tracking system and notification. Both NextCloud & 

Cryptdrive also have spreadsheets which also work for tasks. 
CryptDrive https://opensourcemusings.com/managing-your-tasks-with-nextcloud-tasks

### research data storage
Data storage location for results (surveys, interviews, etc)
Survey and storing results: Both NextCloud and Cryptpad have survey feature which integrates into the drive.
- NextCloud Surveys: https://nextcloud.com/blog/nextcloud-forms-is-here-to-take-on-gafam/
- Cryptpad forms: https://docs.cryptpad.org/en/user_guide/apps/form.html

#### Raw data/interviews/etc

During the project the files could be stored in NextCloud/Cryptpad and once the project concludes the full archive could be moved to 0xacab.
